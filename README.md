# Twitter NLP Project

## Description
Sentiment analysis for appraising public opinion on presidential elections (2021) through text mining from Twitter

## Objectives
- Create Twitter developer account
- Harvest Tweets related to election
- NLP of accessed tweets using negative and positive keywords
- Analyse results

### Setup and Procedue
- Apply for a developer account
- Install required packages in R
- Connect with Twitter
- Harvest Tweets with R
- Extract Tweet Content
- Clean the harvested text
- Text mining based on the cleaned tweets
- Clean structured and Unstructured data
- Assess results and limitations

## Requirements
- Library(twitteR)
- Library(httr)
- Library(RCurl)
- Library(ROAuth)
- Library(stringr)
- Library(plyr)
- Library(dplyr)
- Library (tm)

## Results overview
To analyze the results, 20,000 tweets were extracted from Twitter using RStudio. The results showed significant positive sentiment towards Donald Trump (27.66%) as compared to Joe Biden (18.39%). However, there were numerous limitations; including repeated tweets, chances of machine learning errors, chosen locality and time constraints, among others. This project was conducted in October, 2020. 
